package utils;

import config.PropertiesManager;
import utils.entities.Letter;
import utils.entities.User;


public class FileManager {
    public static User getUser() {
        return FileReader.readObject(PropertiesManager.getUserFilePath(), User.class);
    }

    public static Letter getLetter() {
        return FileReader.readObject(PropertiesManager.getLetterFilePath(), Letter.class);
    }

    private FileManager() {
    }
}
