package utils;

import factory.DriverProvider;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class WaitUtils {
    private WaitUtils() {
    }

    private static WebDriverWait getWait() {
        return new WebDriverWait(DriverProvider.getDriver(), 50);
    }

    public static WebElement forVisibilityOf(WebElement element) {
        return getWait().ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.visibilityOf(element));
    }


    public static List<WebElement> forVisibilityOfList(List<WebElement> webElementList) {
        return getWait().ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.visibilityOfAllElements(webElementList));
    }

    public static Boolean waitTextToBe(By locator, String text) {
        return getWait().ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.textToBe(locator, text));
    }

    public static WebElement waitForElementPresence(By locator) {
        return getWait()
                .ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public static Boolean forInvisibilityOf(WebElement element) {
        return getWait().ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.invisibilityOf(element));
    }


    public static WebElement elementToBeClickable(WebElement pageElement) {
        return getWait()
                .ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.elementToBeClickable(pageElement));
    }
}
