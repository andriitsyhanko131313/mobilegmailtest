package utils.entities;

import java.util.Random;

public class Letter {
    private String recipient;
    private String subject;
    private String message;

    public Letter(String recipient, String subject, String message) {
        this.recipient = recipient;
        this.subject = subject;
        this.message = message;
    }

    public Letter() {
    }

    public String generateSubject() {
        int leftLimit = 97;
        int rightLimit = 122;
        int targetStringLength = 10;
        subject = new Random().ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return subject;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getSubject() {
        return subject;
    }

    public String getMessage() {
        return message;
    }
}
