package ui.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WaitUtils;

public class MenageAccountsPage extends BasePage {

    @FindBy(xpath = "//*[@text='testsel77@gmail.com']")
    private WebElement accountLogin;

    @FindBy(xpath = "//android.widget.ImageButton[@content-desc='Back']")
    private WebElement backButton;

    public String getLogin() {
        return WaitUtils.forVisibilityOf(accountLogin).getText();
    }

    public void tapBackButton() {
        backButton.click();
    }
}
