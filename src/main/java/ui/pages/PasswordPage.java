package ui.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WaitUtils;

public class PasswordPage extends BasePage {

    private static final Logger log = LogManager.getLogger(PasswordPage.class);

    @FindBy(className = "android.widget.EditText")
    private WebElement passwordField;

    @FindBy(xpath = "//*[@text='Next']")
    private WebElement nextButton;

    @FindBy(xpath = "//*[@text='Show password']")
    private WebElement showPasswordText;

    public void setPasswordField(String password) {
        log.info("Set password field");
        WaitUtils.forVisibilityOf(showPasswordText);
        WaitUtils.forVisibilityOf(passwordField);
        passwordField.sendKeys(password);
    }

    public void clickNextButton() {
        log.info("Tap next");
        WaitUtils.forVisibilityOf(nextButton).click();
    }
}
