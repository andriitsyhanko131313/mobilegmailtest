package ui.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WaitUtils;

public class AcceptPolicyPage extends BasePage {

    private static final Logger log = LogManager.getLogger(AcceptPolicyPage.class);

    @FindBy(xpath = "//*[@text='ACCEPT']")
    private WebElement acceptButton;

    public void clickAcceptButton() {
        log.info("Accept Privat Policy");
        WaitUtils.forVisibilityOf(acceptButton);
        acceptButton.click();
    }
}
