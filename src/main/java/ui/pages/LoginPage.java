package ui.pages;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.WaitUtils;


public class LoginPage extends BasePage {

    private static final Logger log = LogManager.getLogger(LoginPage.class);

    @AndroidFindBy(className = "android.widget.EditText")
    private AndroidElement loginField;

    @AndroidFindBy(xpath = "//*[@text='Next']")
    private AndroidElement nextButton;

    public void setLoginField(String login) {
        log.info("Set login field");
        WaitUtils.forVisibilityOf(loginField);
        loginField.sendKeys(login);
    }

    public void clickNextButton() {
        log.info("Tap Next");
        WaitUtils.forVisibilityOf(nextButton).click();
        WaitUtils.forVisibilityOf(nextButton).click();
        WaitUtils.forVisibilityOf(nextButton).click();
    }
}
