package ui.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WaitUtils;

public class AddAccountsPage extends BasePage {

    private static final Logger log = LogManager.getLogger(AddAccountsPage.class);

    @FindBy(id = "setup_addresses_add_another")
    private WebElement addAddress;

    @FindBy(id = "action_done")
    private WebElement takeMeToGmailButton;

    @FindBy(id = "account_address")
    private WebElement accountAddress;

    @FindBy(id = "account_display_name")
    private WebElement userName;

    @FindBy(id = "avatar")
    private WebElement avatarIcon;


    public void tapAddAddress() {
        log.info("Add an email address");
        WaitUtils.forVisibilityOf(addAddress).click();
    }

    public void tapTakeMeToGmail() {
        log.info("Open account");
        WaitUtils.forVisibilityOf(avatarIcon);
        WaitUtils.forVisibilityOf(takeMeToGmailButton).click();
    }
}
