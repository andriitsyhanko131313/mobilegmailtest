package ui.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WaitUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WelcomePage extends BasePage {

    private static final Logger log = LogManager.getLogger(WelcomePage.class);

    @FindBy(id = "welcome_tour_got_it")
    private WebElement gotItButton;

    @FindBy(id = "setup_addresses_add_another")
    private WebElement addAddress;

    public void tapGotItButton() {
        log.info("Tap GOT IT");
        WaitUtils.forVisibilityOf(gotItButton).click();
    }
}
