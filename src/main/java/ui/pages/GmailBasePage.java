package ui.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WaitUtils;

public class GmailBasePage extends BasePage {

    private static final Logger log = LogManager.getLogger(GmailBasePage.class);

    @FindBy(id = "compose_button")
    private WebElement composeButton;

    @FindBy(className = "android.widget.ImageButton")
    private WebElement navigationButton;

    @FindBy(id = "og_apd_ring_view")
    private WebElement accountIcon;

    @FindBy(xpath = "(//android.widget.ListView/android.widget.LinearLayout)[9]")
    public WebElement sentMessagesList;

    @FindBy(id = "description_text")
    private WebElement informMessage;

    @FindBy(id = "account_name")
    private WebElement accountName;

    @FindBy(xpath = "//*[@text='Manage accounts on this device']")
    private WebElement menageActivity;

    @FindBy(id = "dialog_scrim")
    private WebElement dialogScreen;


    public void tapComposeButton() {
        log.info("Tap Compose");
        WaitUtils.forVisibilityOf(accountIcon);
        WaitUtils.forVisibilityOf(composeButton).click();
    }

    public void tapNavigationButton() {
        log.info("Open navigation panel");
        WaitUtils.forVisibilityOf(accountIcon);
        navigationButton.click();
    }

    public void tapSent() {
        log.info("Open sent messages list");
        WaitUtils.forVisibilityOf(sentMessagesList).click();
    }

    public void tapToAccountIcon() {
        log.info("Open account information");
        WaitUtils.forVisibilityOf(accountIcon).click();
    }

    public void tapMenageAccounts() {
        log.info("Open menage accounts menu");
        WaitUtils.forVisibilityOf(menageActivity).click();
    }

    public Boolean isInformMessageDisplayed() {
        log.info("Wain until text to be 'Sent'");
        WaitUtils.forVisibilityOf(informMessage);
        return WaitUtils.waitTextToBe(By.id("description_text"), "Sent");
    }
}
