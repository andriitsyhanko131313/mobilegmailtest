package ui.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WaitUtils;


public class TermsOfServicePage extends BasePage {

    private static final Logger log = LogManager.getLogger(TermsOfServicePage.class);

    @FindBy(xpath = "//android.view.View[4]/android.view.View/android.widget.Button")
    private WebElement iAgreeButton;

    @FindBy(xpath = "//*[@text='testsel77@gmail.com']")
    private WebElement profileIdentifier;

    public void clickIAgreeButton() {
        log.info("Accept Terms of The Service");
        WaitUtils.waitForElementPresence(By
                .xpath("//android.view.View[4]/android.view.View/android.widget.Button")).click();
        WaitUtils.forVisibilityOf(iAgreeButton).click();
        WaitUtils.forVisibilityOf(iAgreeButton).click();
    }
}
