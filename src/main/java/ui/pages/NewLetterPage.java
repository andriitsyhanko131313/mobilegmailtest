package ui.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WaitUtils;

public class NewLetterPage extends BasePage {

    private static final Logger log = LogManager.getLogger(NewLetterPage.class);

    @FindBy(id = "to")
    private WebElement recipientField;
    @FindBy(id = "subject")
    private WebElement subjectField;
    @FindBy(xpath = "(//android.widget.EditText)[2]")
    private WebElement messageField;
    @FindBy(id = "send")
    private WebElement sentLetterButton;

    @FindBy(id = "from_account_name")
    private WebElement accountName;

    public void setRecipientField(String recipient) {
        log.info("Set recipient field");
        WaitUtils.forVisibilityOf(sentLetterButton);
        recipientField.sendKeys(recipient);
    }

    public void setSubjectField(String subject) {
        log.info("Set subject field");
        subjectField.sendKeys(subject);
    }

    public void setMessageField(String message) {
        log.info("Set message field");
        messageField.sendKeys(message);
    }

    public void tapSendLetterButton() {
        log.info("Send letter");
        sentLetterButton.click();
    }

}
