package ui.actions;


import io.qameta.allure.Step;
import ui.pages.*;
import utils.entities.User;

import javax.inject.Inject;


public class LoginAction {
    @Inject
    private LoginPage loginPage;
    @Inject
    private PasswordPage passwordPage;
    @Inject
    private AddAccountsPage addAccountsPage;
    @Inject
    private AcceptPolicyPage acceptPolicyPage;
    @Inject
    private TermsOfServicePage termsOfServicePage;

    @Step("Log in to account and accept Google Terms of Service and Privacy Policy")
    public void logToAccountAndAcceptPolicy(User user) {
        loginPage.setLoginField(user.getLogin());
        loginPage.clickNextButton();
        passwordPage.setPasswordField(user.getPassword());
        passwordPage.clickNextButton();
        termsOfServicePage.clickIAgreeButton();
        acceptPolicyPage.clickAcceptButton();
    }

    @Step("Open gmail base page")
    public void openAccount() {
        addAccountsPage.tapTakeMeToGmail();
    }
}
