package ui.actions;

import io.qameta.allure.Step;
import ui.pages.GmailBasePage;
import ui.pages.MenageAccountsPage;

import javax.inject.Inject;

public class AccountAction {
    @Inject
    private GmailBasePage gmailBasePage;
    @Inject
    private MenageAccountsPage menageAccountsPage;


    @Step("Get account login")
    public String getAccountLogin() {
        gmailBasePage.tapToAccountIcon();
        gmailBasePage.tapMenageAccounts();
        String login = menageAccountsPage.getLogin();
        menageAccountsPage.tapBackButton();
        return login;
    }
}
