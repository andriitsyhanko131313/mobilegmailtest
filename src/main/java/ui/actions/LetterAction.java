package ui.actions;

import io.qameta.allure.Step;
import ui.pages.GmailBasePage;
import ui.pages.MessagePage;
import ui.pages.NewLetterPage;
import ui.pages.SentMessagesPage;
import utils.entities.Letter;

import javax.inject.Inject;

public class LetterAction {
    @Inject
    private GmailBasePage gmailBasePage;
    @Inject
    private NewLetterPage newLetterPage;
    @Inject
    private SentMessagesPage sentMessagesPage;
    @Inject
    private MessagePage messagePage;

    @Step("Create and send letter")
    public void createAndSendLetter(Letter letter) {
        gmailBasePage.tapComposeButton();
        newLetterPage.setRecipientField(letter.getRecipient());
        newLetterPage.setSubjectField(letter.generateSubject());
        newLetterPage.setMessageField(letter.getMessage());
        newLetterPage.tapSendLetterButton();
    }

    @Step("Open list of sent letters")
    public void openSentMessagesList() {
        gmailBasePage.tapNavigationButton();
        gmailBasePage.tapSent();
    }

    @Step("Verify that inform message is displayed")
    public Boolean isInformMessageDisplayed() {
        return gmailBasePage.isInformMessageDisplayed();
    }

    @Step("Open the first message in list")
    public void openMessage() {
        sentMessagesPage.openLastMessage();
    }

    @Step("Get message subject")
    public String getMessageSubject() {
        return messagePage.getLetterSubject();
    }
}
