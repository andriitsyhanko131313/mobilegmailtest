package ui.actions;

import io.qameta.allure.Step;
import ui.pages.AddAccountsPage;
import ui.pages.SetUpEmailPage;
import ui.pages.WelcomePage;

import javax.inject.Inject;


public class NavigateAction {
    @Inject
    private WelcomePage welcomePage;
    @Inject
    private AddAccountsPage addAccountsPage;
    @Inject
    private SetUpEmailPage setUpEmailPage;

    @Step("Navigate to login page")
    public void navigateToGmailLoginPage() {
        welcomePage.tapGotItButton();
        addAccountsPage.tapAddAddress();
        setUpEmailPage.tapGoogleButton();
    }
}
