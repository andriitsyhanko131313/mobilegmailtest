package asserters;

import io.qameta.allure.Step;
import org.testng.Assert;

public class LetterAsserter {

    @Step("Verify that subjects are equals :[{exp},{actual}]")
    public void compareMessageBySubject(String exp, String actual) {
        Assert.assertEquals(exp, actual,
                String.format("Expected subject [%s], but found is [%s]", exp, actual));
    }
}
