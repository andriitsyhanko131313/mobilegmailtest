package asserters;

import io.qameta.allure.Step;
import org.testng.Assert;

public class LoginAsserter {
    @Step("Verify that log in is successful :[{exp},{actual}]")
    public void isLogInSuccessful(String exp, String actual) {
        Assert.assertEquals(exp, actual,
                String.format("Expected login [%s], but found  [%s]", exp, actual));
    }
}
