package tests;

import asserters.LetterAsserter;
import asserters.LoginAsserter;
import org.testng.Assert;
import org.testng.annotations.Test;
import ui.actions.AccountAction;
import ui.actions.LoginAction;
import ui.actions.NavigateAction;
import ui.actions.LetterAction;
import utils.FileManager;
import utils.entities.Letter;
import utils.entities.User;

import javax.inject.Inject;

public class SendLetterTest extends BaseTest {
    @Inject
    private NavigateAction navigateAction;
    @Inject
    private LoginAction loginAction;
    @Inject
    private LetterAction letterAction;
    @Inject
    private LetterAsserter letterAsserter;
    @Inject
    private LoginAsserter loginAsserter;
    @Inject
    private AccountAction accountAction;

    @Test(description = "Verifies send letter")
    public void sendLetterTestCase() {
        Letter letter = FileManager.getLetter();
        User user = FileManager.getUser();

        navigateAction.navigateToGmailLoginPage();
        loginAction.logToAccountAndAcceptPolicy(user);
        loginAction.openAccount();
        loginAsserter.isLogInSuccessful(user.getLogin(), accountAction.getAccountLogin());

        letterAction.createAndSendLetter(letter);
        Assert.assertTrue(letterAction.isInformMessageDisplayed());

        letterAction.openSentMessagesList();
        letterAction.openMessage();
        letterAsserter.compareMessageBySubject(letter.getSubject(), letterAction.getMessageSubject());
    }
}
