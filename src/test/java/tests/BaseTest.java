package tests;

import config.PropertiesManager;
import factory.DriverProvider;
import listener.GmailTestListener;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Guice;
import org.testng.annotations.Listeners;
import utils.AllureAttachments;


@Listeners(GmailTestListener.class)
@Guice
public class BaseTest {

    @AfterMethod(alwaysRun = true)
    public void closeApp() {
        DriverProvider.closeApp();
        AllureAttachments.addFileToAllure(PropertiesManager.getLogFilePath());
    }
}
