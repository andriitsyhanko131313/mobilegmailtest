#MobileGmailTest  project

created by [Andrii Tsyhanko](https://gitlab.com/andriitsyhanko131313)

#Project Documentation

* [Appium](https://github.com/appium/appium)
* [TestNG](https://testng.org/doc/)
* [Log4j2](https://github.com/apache/logging-log4j2)
* [Allure Report](https://docs.qameta.io/allure/)

###Project Requirements
* [Java 1.8](https://www.oracle.com/ru/java/technologies/javase/javase-jdk8-downloads.html)
* [Maven Apache](https://maven.apache.org/)

##Run Project
`mvn clean test`

##Generate Allure Report
`mvn allure:serve` or `mvn allure:report`

`
